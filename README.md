# mock-data-generator

Proof of Concept about generating mock data for AQS services

- **unique_devices.py**: Generate mock data for device_analytics AQS service
- **mimesis_test-requests.py**: Generate mock data for sample requests
- **mimesis_test-people.py**: Generate mock data about people details (with Locale support)