from mimesis.enums import Gender
from mimesis.keys import maybe
from mimesis.locales import Locale
from mimesis.schema import Field, Fieldset, Schema

field = Field(locale=Locale.ES)
fieldset = Fieldset(locale=Locale.ES)

schema = Schema(
    schema=lambda: {
        'id': field('increment'),
        'uid': field('uuid'),
        'name': field('full_name', gender=Gender.FEMALE),
        'user_type': field('gender'),
        'version': field("version", pre_release=True),
        'timestamp': field('timestamp'),
        'owner': {
            'email': field('person.email', domains=['mimesis.name']),
            'token': field('token_hex'),
            'creator': field('full_name', gender=Gender.FEMALE),
        },
        'description': field('text.word', key=maybe('N/A', probability=0.2)),
        'apps': fieldset(
            'text.word', i=5, key=lambda name: {'name': name, 'id': field('uuid')}
        ),
        'sentence': field('sentence')
    },
    iterations=100,
)
schema.create()
schema.to_csv(file_path='mimesis_test-people.csv')
