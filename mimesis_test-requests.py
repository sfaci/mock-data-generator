"""
Sample code (from https://www.activestate.com/blog/top-10-python-packages-for-creating-synthetic-data/)
"""

import pandas as pd
from mimesis.schema import Field, Schema
from IPython.display import display

_ = Field()
description = (
   lambda: {
       'username': _('username'),
       'timestamp': _('timestamp', posix=False),

       'request': {
           'user_type': _('choice', items=['user', 'bot', 'anonymous']),
           'content_type': _('content_type'),
           'version': _('version'),
           'http_status_code': _('http_status_code'),
           'param1': _('dna_sequence'),
           'param2': _('rna_sequence'),
           'token': _('token_hex'),
           'language': _('language'),
           'error': _('boolean'),
           'operating_system': _('os'),
           'document': _('file_name'),
           'headers': _('http_request_headers')
       },
   }
)

schema = Schema(schema=description, iterations=2500)
res_df = pd.DataFrame(schema.create())
req_df = pd.json_normalize(res_df['request'])
pd.concat([res_df, req_df], axis=1).drop('request', axis=1).head()
# display(res_df.to_string())
res_df.to_csv("mimesis_test-requests.csv")
