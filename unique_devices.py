#!/usr/bin/env python3

import csv
import uuid
import random
import datetime

from dateutil.relativedelta import relativedelta

MOCK_DATA_FILE = "unique_devices.csv"
DOMAIN = "analytics.wikimedia.org"
PROJECTS = ["en.wikipedia"]
ACCESS_SITES = ["all-sites", "desktop-site", "mobile-site"]
GRANULARITIES = ["monthly", "daily"]
DATE_FROM = datetime.date(2020, 1, 1)
DATE_TO = datetime.date(2023, 4, 1)
UUID = str(uuid.uuid4())

mock_data_file = open(MOCK_DATA_FILE, "w")
csv_writer = csv.writer(mock_data_file)

print("Generating unique_devices mock data . . .")
for project in PROJECTS:
    for access_site in ACCESS_SITES:
        for granularity in GRANULARITIES:
            date = DATE_FROM
            while date < DATE_TO:
                devices = random.randint(10000000, 90000000)
                offset = random.randint(10000000, 90000000)
                underestimated = random.randint(10000000, 90000000)

                row = [DOMAIN, project, access_site, granularity, date.strftime("%Y%m%d"), UUID, "",
                       devices, offset, underestimated]
                csv_writer.writerow(row)

                if granularity == "daily":
                    date += datetime.timedelta(days=1)
                if granularity == "monthly":
                    date += relativedelta(months=1)

print("Done!")
